public class BTree implements IBTree{

    private Node root = null;

    @Override
    public void print() {
        printNode(this.root);
        System.out.println();
    }

    private void printNode(Node node) {
        if (node == null) {
            return;
        }

        printNode(node.left);
        System.out.print(node.value + ", ");
        printNode(node.right);
    }

    @Override
    public void init(int[] ar) {
        if (ar == null) {
            ar = new int[0];
        }
        for (int value : ar) {
            add(value);
        }
    }

    @Override
    public void clear() {
        this.root=null;
    }

    @Override
    public int depth() {
        Counter counter = new Counter();
        return nodeSize(this.root, counter);
    }


    private int nodeSize(Node node, Counter counter) {
        if (node == null) {
            return 0;
        }

        nodeSize(node.left, counter);
        counter.index++;
        nodeSize(node.right, counter);
        return counter.index;
    }

    @Override
    public int[] toArray() {
        int[] arr = new int[depth()];
        nodeToArray(this.root, arr, new Counter());
        return arr;
    }

    private void nodeToArray(Node node, int[] arr, Counter counter) {
        if (node == null) {
            return;
        }

        nodeToArray(node.left, arr, counter);
        arr[counter.index++] = node.value;
        nodeToArray(node.right, arr, counter);
    }

    @Override
    public void add(int val) {
        if (this.root == null) {
            this.root = new Node(val);
            return;
        }
        addNode(this.root, val);
    }

    private void addNode(Node node, int value) {
        if (value < node.value) {
            if (node.left == null) {
                node.left = new Node(value);
            } else {
                addNode(node.left, value);
            }
        } else {
            if (node.right == null) {
                node.right = new Node(value);
            } else {
                addNode(node.right, value);
            }
        }
    }

    @Override
    public void del(int val) {
        root = delNode(this.root, val);
    }

    private Node delNode(Node node, int val) {
        if (node == null) {
            return null;
        }
        if (val == node.value) {
            if (node.left == null && node.right == null) {
                return null;
            }
            if (node.right == null) {
                return node.left;
            }

            if (node.left == null) {
                return node.right;
            }
            int smallestValue = findSmallestValue(node.right);
            node.value = smallestValue;
            node.right = delNode(node.right, smallestValue);
            return node;

        }
        if (val < node.value) {
            node.left = delNode(node.left, val);
            return node;
        }
        node.right = delNode(node.right, val);
        return node;
    }

    private int findSmallestValue(Node root) {
        return root.left == null ? root.value : findSmallestValue(root.left);
    }

    @Override
    public int getWidth() {
        int width = 0;
        int maxWidth;
        int h = getHeight();

        for (int i = 1; i <= h; i++) {
            maxWidth = getMaxWidth(this.root, i);
            if (maxWidth > width)
                width = maxWidth + 1;
        }
        return width;

    }


    private int getMaxWidth(Node node, int level) {
        if (node == null) {
            return 0;
        }
        if (level == 1) {
            return 1;
        } else if (node.right != null || node.left != null) {
            return getMaxWidth(node.left, level - 1)
                    + getMaxWidth(node.right, level - 1);
        }
        return 0;
    }

    @Override
    public int getHeight() {
        return getDepth(this.root);
    }

    private int getDepth(Node node) {
        if (node == null) {
            return 0;
        }
        return Math.max(getDepth(node.left), getDepth(node.right)) + 1;
    }

    @Override
    public int nodes() {
        return getNodesCount(this.root, new Counter());
    }

    private int getNodesCount(Node node, Counter counter) {
        if (node == null) {
            return 0;
        }
        if (node.left != null || node.right != null) {
            getNodesCount(node.left, counter);
            counter.index++;
            getNodesCount(node.right, counter);
            return counter.index + 1;
        }
        return 0;
    }

    @Override
    public int leaves() {
        return getLeavesCount(this.root, new Counter());
    }

    private int getLeavesCount(Node node, Counter counter) {
        if (node == null) {
            return 0;
        }
        getLeavesCount(node.left, counter);
        if ((node.left == null) && (node.right == null))
            counter.index++;
        getLeavesCount(node.right, counter);
        return counter.index + 1;
    }

    @Override
    public void reverse() {
        reverse(this.root);
    }

    private void reverse(Node node) {
        Node temp = node.right;
        node.right = node.left;
        node.left = temp;
        if (node.left != null) {
            reverse(node.left);
        }
        if (node.right != null) {
            reverse(node.right);}
    }

    private class Node {
        int value;
        Node left;
        Node right;

        Node(int value) {
            this.value = value;
            this.left = null;
            this.right = null;
        }
    }

    private class Counter {
        int index = 0;
    }

}
