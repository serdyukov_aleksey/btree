
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class BTreeTest {

    private static BTree tree;

    @BeforeAll
    static void setUp() {
        tree = new BTree();
    }

    @BeforeEach
    void clearTree() {
        tree.clear();
    }

    @Test
    void test_print1() {
        int[] array = new int[]{5, -15, 20, 35, 0, 6, -10, 25};
        tree.init(array);
        tree.print();
    }

    @Test
    void test_add_1() {
        int[] array = new int[]{5, -15, 20, 35, 0, 6, -10, 25};
        int[] expected = new int[]{-20, -15, -12, -10, 0, 5, 6, 20, 25, 28, 35};
        tree.init(array);

        tree.add(28);
        tree.add(-12);
        tree.add(-20);
        int[] actual = tree.toArray();

        Assertions.assertArrayEquals(expected, actual);
    }

    @Test
    void test_size_1() {
        int[] array = new int[]{5, -15, 20, 35, 0, 6, -10, 25};
        tree.init(array);
        int actual = tree.depth();

        Assertions.assertEquals(8, actual);
    }

    @Test
    void test_clear() {
        int[] array = new int[]{5, -15, 20, 35, 0, 6, -10, 25};
        tree.init(array);
        tree.print();

        tree.clear();
        int actual = tree.depth();
        Assertions.assertEquals(0, actual);
    }

    @Test
    void test_del() {
        int[] array = new int[]{5, -15, 20, 35, 0, 6, -10, 25};
        tree.init(array);
        tree.print();

        tree.del(0);
        tree.print();

        int[] actual = tree.toArray();
        int[] expected = new int[]{-15, -10, 5, 6, 20, 25, 35};
        Assertions.assertArrayEquals(expected, actual);
    }

    @Test
    void test_getHeight() {
        int[] array = new int[]{5, -15, 20, 35, 0, 6, -10, 25};
        tree.init(array);
        int actual = tree.getHeight();

        Assertions.assertEquals(4, actual);
    }

    @Test
    void test_getWidth() {
        int[] array = new int[]{5, -15, 20, 35, 0, 6, -10, 25,30,40,4, -20};
        tree.init(array);
        int actual = tree.getWidth();

        Assertions.assertEquals(5, actual);
    }
    @Test
    void test_Nodes() {
        int[] array = new int[]{5, -15, 20, 35, 0, 6, -10, 25, 30, 40};
        tree.init(array);
        int actual = tree.nodes();

        Assertions.assertEquals(7, actual);
    }

    @Test
    void test_Leaves() {
        int[] array = new int[]{5, -15, 20, 35, 0, 6, -10, 25, 30, 40};
        tree.init(array);
        int actual = tree.leaves();

        Assertions.assertEquals(5, actual);
    }

    @Test
    void test_Revers() {
        int[] array = new int[]{5, -15, 20, 35, 0, 6, -10, 25, 30, 40};
        tree.init(array);
        tree.print();

        tree.reverse();
        tree.print();

        int [] actual = tree.toArray();
        int [] expected = new int [] {40, 35, 30, 25, 20, 6, 5, 0, -10, -15};
        Assertions.assertArrayEquals(expected, actual);
    }

}